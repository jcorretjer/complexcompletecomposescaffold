package com.retroroots.util

import com.retroroots.util.ext.getDayOfTheWeek
import com.retroroots.util.ext.getMonthName
import com.retroroots.util.ext.toDate
import com.retroroots.util.ext.toFirebaseTimestamp
import com.retroroots.util.ext.toSimpleString
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotSame
import org.junit.Test
import java.util.Calendar

class DateExtTest
{
    @Test
    fun toSimpleStringWithValidDateTest()
    {
        assertEquals("06/11/2023", "06/11/2023".toDate()?.toSimpleString())
    }

    @Test
    fun getMonthDateWithValidDateTest()
    {
        assertEquals("Jan", "01/11/2023".toDate()?.getMonthName())
    }

    @Test
    fun errorWhenGetMonthDateWithValidDateTest()
    {
        assertNotSame("Feb", "01/11/2023".toDate()?.getMonthName())
    }

    @Test
    fun getDayOfTheWeekWithValidDateTest()
    {
        assertEquals(Calendar.SATURDAY, "03/16/2024".toFirebaseTimestamp()?.getDayOfTheWeek())
    }
}