package com.retroroots.util

import com.retroroots.util.ext.toDate
import com.retroroots.util.ext.toSimpleString
import junit.framework.TestCase.assertEquals
import org.junit.Test

class DateExtTest
{
    @Test
    fun toSimpleStringWithValidDateTest()
    {
        assertEquals("06/11/2023", "06/11/2023".toDate()?.toSimpleString())
    }
}