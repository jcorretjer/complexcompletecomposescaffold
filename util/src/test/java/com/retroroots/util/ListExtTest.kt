package com.retroroots.util

import com.retroroots.util.ext.addOrRemove
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class ListExtTest
{
    private val list = mutableListOf<Int>()

    private val fakeItem = 1

    private fun tryAddingItem()
    {
        list.addOrRemove(fakeItem){
            it == fakeItem
        }
    }

    @Test
    fun updateListSuccessfullyAddsTest()
    {
        tryAddingItem()

        assertTrue(list.contains(fakeItem))
    }

    @Test
    fun updateListSuccessfullyRemovesTest()
    {
        tryAddingItem()

        tryAddingItem()

        assertFalse(list.contains(fakeItem))
    }
}