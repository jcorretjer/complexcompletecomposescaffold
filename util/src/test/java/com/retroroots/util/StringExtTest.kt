package com.retroroots.util

import com.retroroots.util.ext.toDate
import com.retroroots.util.ext.toFirebaseTimestamp
import com.retroroots.util.ext.toSimpleString
import junit.framework.TestCase.assertEquals
import org.junit.Test

class StringExtTest
{
    @Test
    fun toDateWithValidDateTest()
    {
        assertEquals("Sun Jun 11 00:00:00 EDT 2023", "06/11/2023".toDate()?.toString())
    }

    @Test
    fun toFirebaseTimestampDateWithValidDateTest()
    {
        assertEquals("06/11/2023", "06/11/2023".toFirebaseTimestamp()?.toDate()?.toSimpleString())
    }

    @Test
    fun toFirebaseTimestampDateWithInvalidDateTest()
    {
        assertEquals(null, "asd".toFirebaseTimestamp())
    }
}