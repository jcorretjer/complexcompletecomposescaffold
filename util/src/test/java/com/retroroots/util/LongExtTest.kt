package com.retroroots.util

import com.retroroots.util.ext.milliToDateString
import junit.framework.TestCase.assertEquals
import org.junit.Test

class LongExtTest
{
    @Test
    fun toDateStringWithValidMilliDate()
    {
        assertEquals((1686441600000L).milliToDateString(), "06/11/2023")
    }
}