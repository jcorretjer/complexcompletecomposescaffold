package com.retroroots.util

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test

class PeekableListTest
{
    private lateinit var list: PeekableList<Int>

    @Before
    fun init()
    {
        list = PeekableList(listOf(1,2,3))
    }

    @Test
    fun previousExistTest()
    {
        list.apply {
            updatePeekPointer(1)

            assertEquals(1, peekPrevious())

            updatePeekPointer(1)

            assertEquals(2, peekPrevious())
        }
    }

    @Test
    fun previousDoesNotExistTest()
    {
        assertNull(list.peekPrevious())
    }

    @Test
    fun nextDoesNotExistTest()
    {
        list.apply {
            updatePeekPointer(2)

            assertNull(peekNext())
        }
    }

    @Test
    fun nextExistTest()
    {
        list.apply {

            assertEquals(2, peekNext())

            updatePeekPointer(1)

            assertEquals(3, peekNext())
        }
    }

    @Test
    fun currentExistTest()
    {
        list.apply {
            assertEquals(1, peekCurrent())

            updatePeekPointer(1)

            assertEquals(2, peekCurrent())

            updatePeekPointer(1)

            assertEquals(3, peekCurrent())
        }
    }

    @Test
    fun currentDoesNotExistTest()
    {
        list.apply {
            updatePeekPointer(-1)

            assertNull(peekCurrent())

            updatePeekPointer(10)

            assertNull(peekCurrent())
        }
    }

    @Test
    fun updatePeekPointerByOneTest()
    {
        assertEquals(2, list.updatePeekPointer(1))
    }

    @Test
    fun updatePeekPointerByMinusOneTest()
    {
        list.updatePeekPointer(1)

        assertEquals(1, list.updatePeekPointer(-1))
    }

    private val listOfList = PeekableList(listOf(listOf(1,2), listOf(3), listOf(4), listOf(5,6), listOf(7)))

    private fun advanceAndAssertAllPeeks(advanceBy : Int, next : Int? = null, prev : Int? = null, current : Int? = null)
    {
        listOfList.updatePeekPointer(advanceBy)

        assertTrue(listOfList.peekCurrent()?.first() == current)

        assertTrue(listOfList.peekPrevious()?.first() == prev)

        assertTrue(listOfList.peekNext()?.first() == next)
    }

    @Test
    fun complexListOfListTest()
    {
        //3
        advanceAndAssertAllPeeks(1, 4, 1, 3)

        //4
        advanceAndAssertAllPeeks(1, 5, 3, 4)

        //5
        advanceAndAssertAllPeeks(1, 7, 4, 5)

        //7
        advanceAndAssertAllPeeks(1, prev = 5, current = 7)

        //5
        advanceAndAssertAllPeeks(-1, 7, 4, 5)
    }
}