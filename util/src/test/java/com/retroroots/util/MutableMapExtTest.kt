package com.retroroots.util

import com.retroroots.util.ext.addOrRemove
import com.retroroots.util.ext.remove
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.After
import org.junit.Test

class MutableMapExtTest
{
    private val map = mutableMapOf<String, Int>()

    private val testValue = 1

    @After
    fun fin()
    {
        map.clear()
    }

    @Test
    fun addORemoveAddTest()
    {
        map.addOrRemove(testValue, "1"){
            it.value == testValue
        }

        assertEquals("{1=1}", map.toString())
    }

    @Test
    fun addORemoveRemoveTest()
    {
        addORemoveAddTest()

        map.apply {
            addOrRemove(testValue, "1"){
            it.value == testValue
        }

            assertTrue(isEmpty())
        }
    }

    @Test
    fun removeExistingItemTest()
    {
        addORemoveAddTest()

        map.apply {
            remove { it.value == 1 }

            assertTrue(isEmpty())
        }
    }
}