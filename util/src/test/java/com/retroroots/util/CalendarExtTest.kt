package com.retroroots.util

import com.retroroots.util.ext.currentDate
import com.retroroots.util.ext.toSimpleString
import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.util.Calendar

class CalendarExtTest
{
    @Test
    fun currentDateTest()
    {
        //Pass the current date as expected
        assertEquals("06/13/2023", Calendar.getInstance().currentDate().toSimpleString())
    }

    @Test
    fun currentDatePlus1Test()
    {
        //Pass the current date as expected
        assertEquals("06/14/2023", Calendar.getInstance().currentDate(1).toSimpleString())
    }

    @Test
    fun currentDateMinus1Test()
    {
        //Pass the current date as expected
        assertEquals("03/19/2024", Calendar.getInstance().currentDate(-1).toSimpleString())
    }
}