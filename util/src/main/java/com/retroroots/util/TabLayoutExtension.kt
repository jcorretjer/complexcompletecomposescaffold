package com.retroroots.util

import com.google.android.material.tabs.TabLayout

fun TabLayout.selectTab(index : Int)
{
    selectTab(getTabAt(index))
}