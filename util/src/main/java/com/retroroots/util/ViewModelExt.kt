package com.retroroots.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

private const val SUBSCRIBED_TIMEOUT = 5000L

fun <FlowType> ViewModel.simplifiedStateIn(
    flow : Flow<FlowType>,
    started: SharingStarted = SharingStarted.WhileSubscribed(SUBSCRIBED_TIMEOUT),
    initial : FlowType
) = flow.stateIn(viewModelScope, started, initial)

fun ViewModel.viewModelScopeLauncher(dispatcher: CoroutineDispatcher = Dispatchers.IO, block : suspend CoroutineScope.() -> Unit)
{
    viewModelScope.launch(dispatcher, block = block)
}