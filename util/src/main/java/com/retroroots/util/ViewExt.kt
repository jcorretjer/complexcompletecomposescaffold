package com.retroroots.util

import android.view.View

fun View.toggleVisibility(visible : Boolean)
{
    visibility =
            if (visible)
                View.VISIBLE
            else
                View.GONE
}