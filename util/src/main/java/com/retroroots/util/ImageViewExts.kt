package com.retroroots.util

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Base64
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setSrcFromBase64(base64String : String, activity: Activity, errorDrawable: Drawable)
{
    Base64.decode(base64String, Base64.DEFAULT).let {
        setSrc(activity, errorDrawable, BitmapFactory.decodeByteArray(it, 0, it.size))
    }
}

fun ImageView.setSrc(context: Context, errorDrawable: Drawable, bitmap: Bitmap?)
{
    Glide.with(context)
        .load(bitmap)
        .error(errorDrawable)
        .into(this)
}

fun ImageView.setSrc(context: Context, errorDrawable: Drawable, uri: Uri?)
{
    Glide.with(context)
        .load(uri)
        .error(errorDrawable)
        .into(this)
}