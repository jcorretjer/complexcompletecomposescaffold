package com.retroroots.util

/**
 * List that allows you to peer to the next or previous item without changing the position of the index
 */
class PeekableList<ListType>(private val list: List<ListType>) : List<ListType> by list {

    private var index = 0

    /**
     * @return The previous item on the list without advancing.
     */
    fun peekPrevious(): ListType? =
            if (index > 0 && index < list.size)
                list[index - 1]

            else
                null

    /**
     * @return The next item on the list without advancing.
     */
    fun peekNext(): ListType? =
            if (index >= 0 && index < list.size - 1)
                list[index + 1]

            else
                null


    /**
     * @return The current item on the list without advancing.
     */
    fun peekCurrent(): ListType? =
            if (index >= 0 && index < list.size)
                list[index]

            else
                null

    /**
     * Advances to any direction based on the provided value
     * @return Item in the new location
     */
    fun updatePeekPointer(value : Int): ListType? = (
            try
            {
                list[index + value]
            }

            catch (_: Exception)
            {
                null
            }
            ).apply {
            index += value
        }
}