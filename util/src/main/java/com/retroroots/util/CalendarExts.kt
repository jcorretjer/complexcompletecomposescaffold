package com.retroroots.util

import com.google.firebase.Timestamp
import java.util.Calendar
import java.util.Date

fun Calendar.currentDate(daysToAdd: Int = 0): Date
{
    time = Timestamp.now().toDate()

    add(Calendar.DAY_OF_YEAR, daysToAdd)

    return time
}
