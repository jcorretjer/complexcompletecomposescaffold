package com.retroroots.util

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

fun Date.toSimpleString(pattern: String = "MM/dd/yyyy"): String = SimpleDateFormat(pattern, Locale.getDefault()).format(this)

fun Date.toCalendar() = Calendar.getInstance().let {
    it.time = this

    it
}