package com.retroroots.util

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun String.toDate(pattern: String = "MM/dd/yyyy"): Date? = SimpleDateFormat(pattern, Locale.getDefault()).parse(this)