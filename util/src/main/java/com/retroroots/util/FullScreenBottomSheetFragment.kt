package com.retroroots.util

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class FullScreenBottomSheetFragment : BottomSheetDialogFragment()
{
    private lateinit var behavior : BottomSheetBehavior<FrameLayout>

    override fun onStart() {
        super.onStart()

        //Forces the height to match the parent thus turning it into a full screen
        (requireView().parent as ViewGroup).apply {
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Needed to enable the view to be scrolled up further thus fixing the layout height problem where the keyboard blocks part of the bottom.
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogNoFloating)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Config behavior to fit 100% the layout height while also preventing the peek height from stopping the collapse
        (requireDialog() as BottomSheetDialog).behavior.apply {
            state = BottomSheetBehavior.STATE_EXPANDED

            peekHeight = resources.displayMetrics.heightPixels

            skipCollapsed = true

            this@FullScreenBottomSheetFragment.behavior = this
        }
    }

    fun toggleSwipeDismiss(enabled : Boolean)
    {
        behavior.isDraggable = enabled
    }

    fun updateBotSheetBehaviorState(behaviorState : Int)
    {
        behavior.state = behaviorState
    }
}