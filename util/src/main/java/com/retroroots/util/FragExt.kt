package com.retroroots.util

import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

fun <F> Fragment.launchAndCollect(flow : Flow<F>, dispatcher: CoroutineDispatcher = Dispatchers.Default, onCollect : suspend (F) -> Unit)
{
    viewLifecycleOwner.lifecycleScope.launch(dispatcher) {
        flow.collect(onCollect)
    }
}