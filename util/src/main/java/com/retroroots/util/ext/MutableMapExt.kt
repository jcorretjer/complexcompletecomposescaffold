package com.retroroots.util.ext

/**
 * Adds item, if not found. Removes item, if found.
 *
 * @return Removed item or null, if item added because it was not found.
 */
fun <Key, Value> MutableMap<Key, Value>.addOrRemove(item : Value, addKey : Key, removePredicate : (Map.Entry<Key, Value>) -> Boolean) =
        remove(removePredicate) ?: put(addKey, item)


fun <Key, Value> MutableMap<Key, Value>.remove(removePredicate : (Map.Entry<Key, Value>) -> Boolean) =
        remove(asSequence().firstOrNull(removePredicate)?.key)