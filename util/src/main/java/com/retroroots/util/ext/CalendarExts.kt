package com.retroroots.util.ext

import com.google.firebase.Timestamp
import java.util.Calendar
import java.util.Date

fun Calendar.currentDate(daysToAdd: Int = 0): Date
{
    time = Timestamp.now().toDate()

    addDays(daysToAdd)

    return time
}

fun Calendar.addDays(daysToAdd: Int = 0)
{
    add(Calendar.DAY_OF_YEAR, daysToAdd)
}

fun Calendar.toTimestamp() = Timestamp(time)
