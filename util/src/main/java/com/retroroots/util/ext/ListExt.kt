package com.retroroots.util.ext

import com.google.common.collect.Iterables.removeIf
import com.retroroots.util.PeekableList

fun <I> MutableList<I>.addOrRemove(item : I, removePredicate: com.google.common.base.Predicate<I>)
{
    if(removeIf(asIterable(), removePredicate).not())
        add(item)
}

fun <ElementType> List<ElementType>.toPeekableList() = PeekableList(this)