package com.retroroots.util.ext

import android.content.Context
import android.net.Uri
import android.util.Base64

fun Uri.toBase64(context: Context): String =
        with(context.contentResolver.openInputStream(this)) {
            this?.let {
                return Base64.encodeToString(it.readBytes(), Base64.DEFAULT)
            }
                ?: return ""
        }