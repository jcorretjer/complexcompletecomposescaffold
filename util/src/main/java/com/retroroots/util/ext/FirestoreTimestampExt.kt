package com.retroroots.util.ext

import com.google.firebase.Timestamp
import java.util.Calendar

fun Timestamp.toSimpleDateString(datePattern: String = "MM/dd/yyyy") = toDate().toSimpleString(datePattern)

fun Timestamp.getMonthName() = toDate().getMonthName()

fun Timestamp.getDayOfTheWeek() =
        toDate().toCalendar()
    .get(Calendar.DAY_OF_WEEK)