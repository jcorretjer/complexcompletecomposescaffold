package com.retroroots.util.ext

import java.text.SimpleDateFormat
import java.util.*

/**
 * Only works for milliseconds
 */
fun Long.milliToDateString(): String = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).apply {
    timeZone = TimeZone.getTimeZone("UTC")
}.format(Date(this))