package com.retroroots.util.ext

import android.view.View

/**
 * Toggles between visible and gone.
 */
fun View.toggleVisibility(visible : Boolean)
{
    visibility =
            if (visible)
                View.VISIBLE

            else
                View.GONE
}

/**
 * Toggles between visible and invisible.
 */
fun View.toggleInvisible(visible: Boolean)
{
    visibility =
            if (visible)
                View.VISIBLE

            else
                View.INVISIBLE
}