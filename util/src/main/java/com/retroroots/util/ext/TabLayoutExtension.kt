package com.retroroots.util.ext

import com.google.android.material.tabs.TabLayout

fun TabLayout.selectTab(index : Int)
{
    selectTab(getTabAt(index))
}