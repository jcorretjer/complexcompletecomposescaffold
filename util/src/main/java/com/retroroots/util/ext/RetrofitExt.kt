package com.retroroots.util.ext

import okhttp3.ResponseBody

fun ResponseBody?.responseBodyToString() = this?.charStream()?.readText().toString()