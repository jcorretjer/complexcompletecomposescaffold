package com.retroroots.util.ext

import android.text.format.DateFormat
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

fun Date.toSimpleString(datePattern: String = "MM/dd/yyyy"): String = SimpleDateFormat(datePattern, Locale.getDefault()).format(this)

fun Date.toCalendar() = Calendar.getInstance().let {
    it.time = this

    it
}

fun Date.getMonthName() = DateFormat.format("MMM", this).toString()

fun Date.toTimestamp() = Timestamp(this)