package com.retroroots.util.ext

import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

fun String.toDate(pattern: String = "MM/dd/yyyy"): Date? =
    try{
        SimpleDateFormat(pattern, Locale.getDefault()).parse(this)
    }

    catch (_:Exception)
    {
        null
    }

fun String.toFirebaseTimestamp() = toDate()?.let {
    Timestamp(it)
} ?: run {
        null
    }

fun String.tryToCalendar() =
    try
    {
        Calendar.getInstance().apply {
            toDate()?.let {
                time = it
            }
        }
    }

    catch (_ :Exception)
    {
        null
    }

/**
 * @return Null if conversion failed
 */
fun String.toFirebaseTimestamp(daysToAdd : Int) = tryToCalendar()?.let {
    it.addDays(daysToAdd)

    it
}?.toTimestamp()

