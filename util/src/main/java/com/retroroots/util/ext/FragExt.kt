package com.retroroots.util.ext

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

fun <F> Fragment.launchAndCollect(flow : Flow<F>, dispatcher: CoroutineDispatcher = Dispatchers.Default, onCollect : FlowCollector<F>)
{
    viewLifecycleOwner.lifecycleScope.launch(dispatcher) {
        flow.collect(onCollect)
    }
}

fun <FlowType> Fragment.launchAndCollectLatestFlow(
    flow: Flow<FlowType>,
    dispatcher: CoroutineDispatcher = Dispatchers.Default,
    onCollect: suspend (FlowType) -> Unit
)
{
    viewLifecycleOwner.lifecycleScope.launch(dispatcher) {
        repeatOnLifecycle(Lifecycle.State.STARTED) {
            flow.collectLatest(onCollect)
        }
    }
}