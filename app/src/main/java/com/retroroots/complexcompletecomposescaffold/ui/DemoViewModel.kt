package com.retroroots.complexcompletecomposescaffold.ui

import androidx.lifecycle.ViewModel
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo.SharedDemoRepo
import com.retroroots.util.viewModelScopeLauncher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

@HiltViewModel
class DemoViewModel @Inject constructor(
    private val repository: SharedDemoRepo,
    private val dispatcher: CoroutineDispatcher
) : ViewModel()
{
    //If I wanted to turn this into a stateflow use stateIn where the flow stream is filtered by .distinctUntilChanged()
    val selectAll = repository.selectAll()

    fun createRecord(demo: DemoData)
    {
        viewModelScopeLauncher(dispatcher) {
            repository.createRecord(demo)
        }
    }
}