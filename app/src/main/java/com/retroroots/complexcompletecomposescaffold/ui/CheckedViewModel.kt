package com.retroroots.complexcompletecomposescaffold.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.SharedCheckedRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckedViewModel @Inject constructor(
    private val checkedRepository: SharedCheckedRepo,
    private val dispatcher: CoroutineDispatcher
) : ViewModel()
{
    fun getCheckedByDemoAndUserId(demoId: Int, userId: Int) =
            checkedRepository.getCheckedByDemoAndUserId(demoId, userId)

    fun createCheckedRecord(checkedData: CheckedData)
    {
        viewModelScope.launch(dispatcher) {
            checkedRepository.createCheckedRecord(checkedData)
        }
    }

    fun updateCheckedRecord(checkedData: CheckedData)
    {
        viewModelScope.launch(dispatcher) {
            checkedRepository.updateCheckedRecord(checkedData)
        }
    }
}