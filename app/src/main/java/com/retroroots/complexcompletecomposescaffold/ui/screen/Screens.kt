package com.retroroots.complexcompletecomposescaffold.ui.screen

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.Card
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.lifecycle.flowWithLifecycle
import com.retroroots.complexcompletecomposescaffold.R
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.FakeCheckedRepo
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.ui.CheckedViewModel
import com.retroroots.complexcompletecomposescaffold.ui.theme.Purple40
import com.retroroots.complexcompletecomposescaffold.ui.theme.PurpleGrey80
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest

private const val userID = 1

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeToolbar(context: Context)
{
    CenterAlignedTopAppBar(
        title = {
            Text(text = "home")
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = PurpleGrey80,
            titleContentColor = Purple40
        ),
        navigationIcon = {
            IconButton(onClick =
            {
                Toast.makeText(context, "nav icon clicked", Toast.LENGTH_SHORT)
                    .show()
            })
            {
                Icon(
                    imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                    contentDescription = null
                )
            }
        },
        actions = {
            IconButton(onClick = {
                Toast.makeText(context, "menu clicked", Toast.LENGTH_SHORT)
                    .show()
            }) {
                Icon(
                    imageVector = Icons.Filled.Menu,
                    contentDescription = null
                )
            }
        }
    )
}

@Composable
fun BotNavBar(context: Context)
{
    NavigationBar {
        NavigationBarItem(
            selected = true,
            onClick = {
                Toast.makeText(context, "bot btn 1 clicked", Toast.LENGTH_SHORT)
                .show()
                      },
            icon = {
                Icon(Icons.Filled.Done, null)
            }
        )

        NavigationBarItem(
            selected = false,
            onClick = {
                Toast.makeText(context, "bot btn 2 clicked", Toast.LENGTH_SHORT)
                    .show()
            },
            icon = {
                Icon(Icons.Filled.Build, null)
            }
        )
    }
}

@Composable
fun MainScaffold(
    context: Context,
    demos: DatabaseResult<List<DemoData>>,
    onActionBtnClicked: () -> Unit,
    forEachDemo: @Composable (demoId: Int) -> CheckedViewModel
)
{
    //val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        topBar = {
            HomeToolbar(context = context)
        },
        bottomBar = {
            BotNavBar(context = context)
        },
        floatingActionButton = {
            FloatingActionButton(onActionBtnClicked) {
                Icon(Icons.Filled.Add, contentDescription = "")
            }
        }
    ) {
        DataList(demos, it, userId = userID, forEachDemo)
    }
}

enum class TestTag
{
    DATA_LIST,
    NOT_FOUND
}

@Composable
fun DataList(
    demoResult: DatabaseResult<List<DemoData>>,
    itemPaddingValues: PaddingValues = PaddingValues(5.dp, 10.dp),
    userId : Int,
    forEachDemo : @Composable (demoId : Int) -> CheckedViewModel
)
{
    val lifecycleOwner = LocalLifecycleOwner.current

    (demoResult as? DatabaseResult.Found)?.data?.let { demos ->
        LazyColumn(
            Modifier
                .fillMaxSize()
                .background(Color.Cyan)
                .testTag(TestTag.DATA_LIST.name)
                .padding(
                    itemPaddingValues.calculateStartPadding(LayoutDirection.Ltr),
                    itemPaddingValues.calculateTopPadding(),
                    itemPaddingValues.calculateStartPadding(LayoutDirection.Ltr)
                )
        ) {
            items(
                demos,
                key = { it.id }
            ) { demo ->
                val checkedViewModel = forEachDemo(demo.id)

                val checkedData by produceState(initialValue = CheckedData.emptyData()) {
                    checkedViewModel.getCheckedByDemoAndUserId(demo.id, userId)
                        .flowWithLifecycle(lifecycleOwner.lifecycle)
                        .collectLatest { result ->
                            (result as? DatabaseResult.Found)?.let {
                                value = it.data
                            }
                        }
                }

                ListItem(demoData = demo, checkedData, itemPaddingValues = itemPaddingValues, userId) {
                    if (it.id == CheckedData.DEFAULT_ID)
                        checkedViewModel.createCheckedRecord(it)

                    else
                        checkedViewModel.updateCheckedRecord(it)
                }
            }
        }
    } ?: run {
            NotFoundItem()
    }
}

@Composable
fun ListItem(
    demoData: DemoData,
    checkedData: CheckedData,
    itemPaddingValues: PaddingValues = PaddingValues(5.dp, 10.dp),
    userId: Int,
    onCheckChanged: (checkedData: CheckedData) -> Unit
)
{
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.Green)
            .padding(bottom = itemPaddingValues.calculateBottomPadding())
    ) {
        ListItemRow(demoData, checkedData, userId, onCheckChanged)
    }
}

@Composable
fun NotFoundItem()
{
    Column(
        Modifier
            .fillMaxSize()
            .background(Color.Cyan)
            .testTag(TestTag.NOT_FOUND.name),
        Arrangement.Center,
        Alignment.CenterHorizontally
    ) {
        Icon(Icons.Filled.Clear, null)
    }
}

@Composable
fun ListItemRow(demoData: DemoData, checkedData: CheckedData, userId: Int, onCheckChanged : (CheckedData) -> Unit)
{
    Row(
        modifier = Modifier
            .padding(5.dp)
            .fillMaxWidth()
            .background(Color.Red),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {

        Text(text = demoData.id.toString())

        Checkbox(
            checked = checkedData.checked,
            onCheckedChange = {
                onCheckChanged(checkedData.copy(checked = it, demoId = demoData.id, userId = userId))
            },
            modifier = Modifier.testTag("${demoData.id}${stringResource(id = R.string.checkBoxTestTegPrefix)}")
        )
    }
}


@Preview
@Composable
fun HomeToolbarPreview()
{
    HomeToolbar(LocalContext.current)
}

@Preview
@Composable
fun MainScaffoldPreview()
{
    MainScaffold(
        context = LocalContext.current,
        DatabaseResult.Found(listOf(DemoData.empty().copy(id = 1))),
        {},
        {CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)}
    )
}

@Preview
@Composable
fun BotNavBarPreview()
{
    BotNavBar(context = LocalContext.current)
}

@Preview
@Composable
fun ListItemPreview()
{
    ListItem(DemoData.empty(), CheckedData.emptyData(), userId = 0){

    }
}

@Preview
@Composable
fun ListItemUncheckedRowPreview()
{
    ListItemRow(DemoData.empty(), CheckedData.emptyData(), userId = 0){

    }
}

@Preview
@Composable
fun ListItemCheckedRowPreview()
{
    ListItemRow(DemoData.empty(), CheckedData.emptyData().copy(checked = true), userId = 0){

    }
}

@Preview
@Composable
fun EmptyDataListPreview()
{
    DataList(DatabaseResult.NotFound, userId = 0, forEachDemo = {CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)})
}

@Preview
@Composable
fun DataList1ItemPreview()
{
    DataList(
        DatabaseResult.Found(listOf(DemoData.empty().copy(id = 1))),
        userId = 0,
        forEachDemo = {CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)}
    )
}

@Preview
@Composable
fun DataList2ItemPreview()
{
    DataList(
        DatabaseResult.Found(
            listOf(
                DemoData.empty().copy(id = 1),
                DemoData.empty().copy(id = 2)
            )
        ),
        userId = 0,
        forEachDemo = {CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)}
    )
}

@Preview
@Composable
fun DataListManyItemPreview()
{
    DataList(
        DatabaseResult.Found(
            listOf(
                DemoData.empty().copy(id = 1),
                DemoData.empty().copy(id = 2),
                DemoData.empty().copy(id = 3),
                DemoData.empty().copy(id = 4),
                DemoData.empty().copy(id = 5),
                DemoData.empty().copy(id = 6),
                DemoData.empty().copy(id = 7),
                DemoData.empty().copy(id = 8),
                DemoData.empty().copy(id = 9),
                DemoData.empty().copy(id = 10),
                DemoData.empty().copy(id = 11),
                DemoData.empty().copy(id = 12),
                DemoData.empty().copy(id = 13),
                DemoData.empty().copy(id = 14),
                DemoData.empty().copy(id = 15),
                DemoData.empty().copy(id = 16)
            )
        ),
        userId = 0,
        forEachDemo = {CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)}
    )
}