package com.retroroots.complexcompletecomposescaffold.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.FakeCheckedRepo
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo.FakeDemoRepo
import com.retroroots.complexcompletecomposescaffold.ui.screen.MainScaffold
import com.retroroots.network.data_source.local.DatabaseResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED){

            }
        }

        setContent {
            MainUI(viewModel<DemoViewModel>()){
                viewModel<CheckedViewModel>(key = it.toString())
            }
        }
    }

    @Composable
    fun MainUI(demoViewModel: DemoViewModel, forEachDemo : @Composable (demoId : Int) -> CheckedViewModel)
    {
        MaterialTheme {
            val demoResult by demoViewModel.selectAll.collectAsStateWithLifecycle(DatabaseResult.NotFound)

            LaunchedEffect(key1 = demoResult) {
                println(demoResult.toString())
            }

            val coroutineScope = rememberCoroutineScope()

            val context = LocalContext.current

            MainScaffold(context = context, demoResult, {
                coroutineScope.launch {
                    demoViewModel.createRecord(DemoData.empty())
                }
            }, forEachDemo)
        }
    }

    @Preview
    @Composable
    fun MainUIPreview()
    {
        MainUI(DemoViewModel(FakeDemoRepo(), Dispatchers.IO)) { CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO) }
    }
}