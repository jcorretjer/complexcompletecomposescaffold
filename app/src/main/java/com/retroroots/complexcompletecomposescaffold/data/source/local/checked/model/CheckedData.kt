package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model

data class CheckedData(
    val id : Int,
    var checked : Boolean = false,
    val demoId : Int,
    var userId : Int
)
{
    companion object{
        const val DEFAULT_ID = 0

        fun emptyData() = CheckedData(demoId = 0, id = DEFAULT_ID, userId = 0)
    }

    fun toInternal() = EntityCheckedData(id, checked, demoId, userId)
}
