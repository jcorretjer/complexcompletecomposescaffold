package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.di

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDao
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDatabase
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedLocalDataSource
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.CheckedRepository
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.SharedCheckedRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object CheckedLocalModule
{
    @Singleton
    @Provides
    fun providesCheckedDao(checkedDatabase: CheckedDatabase) = checkedDatabase.demoDao()

    @Singleton
    @Provides
    fun providesCheckedLocalDataSource(checkedDao: CheckedDao) = CheckedLocalDataSource(checkedDao)

    @Singleton
    @Provides
    fun providesCheckedRepo(checkedLocalDataSource: CheckedLocalDataSource) : SharedCheckedRepo =
            CheckedRepository(checkedLocalDataSource)
}