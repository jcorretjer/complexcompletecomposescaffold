package com.retroroots.complexcompletecomposescaffold.data.source.local.checked

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.network.data_source.local.DatabaseResult
import com.retroroots.network.data_source.local.LocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class CheckedLocalDataSource(private val dao: CheckedDao) : LocalDataSource<CheckedData, CheckedData?>
{
    //Can return null, if it's not found
    fun selectByDemoAndUserId(demoId: Int, userId: Int) =
            dao.getChecked(demoId, userId)
                .map { data ->
                    data?.let {
                         DatabaseResult.Found(it.toExternal())
                     } ?: DatabaseResult.NotFound
                }
                .distinctUntilChanged()

    override suspend fun createByObj(obj: CheckedData)
    {
        dao.insertChecked(obj.toInternal())
    }

    override suspend fun updateByObj(obj: CheckedData)
    {
        dao.updateChecked(obj.toInternal())
    }
}