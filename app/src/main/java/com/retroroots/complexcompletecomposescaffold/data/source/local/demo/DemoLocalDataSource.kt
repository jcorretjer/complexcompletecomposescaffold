package com.retroroots.complexcompletecomposescaffold.data.source.local.demo

import com.retroroots.network.data_source.local.DatabaseResult
import com.retroroots.network.data_source.local.LocalDataSource
import kotlinx.coroutines.flow.map

class DemoLocalDataSource(private val dao: DemoDao) : LocalDataSource<DemoData, DemoData?>
{
    /*
     There are alternatives ways to implement this. One way to do this is using calling .distinctUntilChanged()
     on the map flow which filters repeated values. Another way is to add an idle state in the databaseResult class that is the default.
     With that we probably won't need .distinctUntilChanged() because not found is ony dedicated to fail instead of fail and default thus
     maybe fixing the duplicated values on database change and empty results.
     */
    fun selectAll() = dao.getAllDemo()
        .map {
            if (it.isEmpty())
                DatabaseResult.NotFound

            else
                DatabaseResult.Found(it)
        }


    override suspend fun createByObj(obj: DemoData)
    {
        dao.insertDemo(obj)
    }

    override suspend fun updateByObj(obj: DemoData)
    {
        TODO("Not using")
    }
}