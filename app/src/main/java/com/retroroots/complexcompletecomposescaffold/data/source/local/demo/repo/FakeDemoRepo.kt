package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class FakeDemoRepo : SharedDemoRepo
{
    private var demos = mutableListOf<DemoData>()

    private var flow = MutableSharedFlow<DatabaseResult<List<DemoData>>>()

    suspend fun emitFakeDBResult(value : DatabaseResult<List<DemoData>>) = flow.emit(value)

    override fun selectAll(): Flow<DatabaseResult<List<DemoData>>> = flow

    override suspend fun createRecord(demo: DemoData)
    {
        demos.add(demo)

        emitFakeDBResult(DatabaseResult.Found(demos))
    }
}