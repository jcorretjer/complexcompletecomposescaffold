package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoLocalDataSource
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.flow.Flow

class DemoRepository(private val dataSource: DemoLocalDataSource) : SharedDemoRepo
{
    override fun selectAll(): Flow<DatabaseResult<List<DemoData>>> = dataSource.selectAll()

    override suspend fun createRecord(demo: DemoData)
    {
        dataSource.createByObj(demo)
    }
}