package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedLocalDataSource
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.flow.Flow

class CheckedRepository(private val checkedLocalDataSource: CheckedLocalDataSource) : SharedCheckedRepo
{
    override fun getCheckedByDemoAndUserId(demoId: Int, userId: Int): Flow<DatabaseResult<CheckedData>> =
            checkedLocalDataSource.selectByDemoAndUserId(demoId, userId)

    override suspend fun createCheckedRecord(checkedData: CheckedData)
    {
        checkedLocalDataSource.createByObj(checkedData)
    }

    override suspend fun updateCheckedRecord(checkedData: CheckedData)
    {
        checkedLocalDataSource.updateByObj(checkedData)
    }
}