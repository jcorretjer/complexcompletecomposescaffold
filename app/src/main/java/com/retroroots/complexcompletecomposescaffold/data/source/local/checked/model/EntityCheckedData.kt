package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EntityCheckedData(
    @PrimaryKey(true)
    val id : Int,
    val checked : Boolean = false,
    val demoId : Int,
    val userId : Int
)
{
    fun toExternal() = CheckedData(id, checked, demoId, userId)
}
