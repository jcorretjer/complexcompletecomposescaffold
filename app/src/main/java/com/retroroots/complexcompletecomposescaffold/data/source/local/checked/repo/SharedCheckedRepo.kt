package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.flow.Flow

interface SharedCheckedRepo
{
    fun getCheckedByDemoAndUserId(demoId: Int, userId: Int) : Flow<DatabaseResult<CheckedData>>

    suspend fun createCheckedRecord(checkedData: CheckedData)

    suspend fun updateCheckedRecord(checkedData: CheckedData)
}