package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.flow.Flow

interface SharedDemoRepo
{
    fun selectAll(): Flow<DatabaseResult<List<DemoData>>>

    suspend fun createRecord(demo: DemoData)
}