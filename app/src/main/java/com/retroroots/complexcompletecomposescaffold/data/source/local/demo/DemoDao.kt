package com.retroroots.complexcompletecomposescaffold.data.source.local.demo

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface DemoDao
{
    @Query("SELECT * FROM demodata")
    fun getAllDemo() : Flow<List<DemoData>>

    @Insert
    suspend fun insertDemo(demoData: DemoData)
}