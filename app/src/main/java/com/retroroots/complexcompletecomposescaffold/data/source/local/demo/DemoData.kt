package com.retroroots.complexcompletecomposescaffold.data.source.local.demo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DemoData(
    @PrimaryKey(true)
    val id : Int
)
{
    companion object{
        fun empty() = DemoData(0)
    }
}
