package com.retroroots.complexcompletecomposescaffold.data.source.local.checked

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.EntityCheckedData
import kotlinx.coroutines.flow.Flow

@Dao
interface CheckedDao
{
    @Query("SELECT * FROM entitycheckeddata WHERE demoId == :demoId AND userId == :userId")
    fun getChecked(demoId: Int, userId: Int) : Flow<EntityCheckedData>

    @Insert
    suspend fun insertChecked(checkedData: EntityCheckedData)

    @Update
    suspend fun updateChecked(checkedData: EntityCheckedData)
}