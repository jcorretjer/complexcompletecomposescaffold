package com.retroroots.complexcompletecomposescaffold.data.source.local.checked

import androidx.room.Database
import androidx.room.RoomDatabase
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.EntityCheckedData

@Database(entities = [EntityCheckedData::class], version = 1, exportSchema = false)
abstract class CheckedDatabase : RoomDatabase()
{
    abstract fun demoDao() : CheckedDao
}