package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.di

import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoDao
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoDatabase
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoLocalDataSource
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo.DemoRepository
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo.SharedDemoRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DemoLocalModule
{
    @Singleton
    @Provides
    fun providesDemoDao(demoDatabase: DemoDatabase) = demoDatabase.demoDao()

    @Singleton
    @Provides
    fun providesDemoLocalDataSource(demoDao: DemoDao) = DemoLocalDataSource(demoDao)

    @Singleton
    @Provides
    fun providesDemoRepo(demoLocalDataSource: DemoLocalDataSource) : SharedDemoRepo =
            DemoRepository(demoLocalDataSource)
}