package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.network.data_source.local.DatabaseResult
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class FakeCheckedRepo : SharedCheckedRepo
{
    private var flow = MutableSharedFlow<DatabaseResult<CheckedData>>(1, 1, BufferOverflow.DROP_OLDEST)

    suspend fun emitFakeDBResult(value : DatabaseResult<CheckedData>)
    {
        flow.emit(value)
    }

    override fun getCheckedByDemoAndUserId(demoId: Int, userId: Int): Flow<DatabaseResult<CheckedData>> = flow

    override suspend fun createCheckedRecord(checkedData: CheckedData)
    {
        emitFakeDBResult(DatabaseResult.Found(checkedData.copy(id = checkedData.id + 1)))
    }

    override suspend fun updateCheckedRecord(checkedData: CheckedData)
    {
        emitFakeDBResult(DatabaseResult.Found(checkedData))
    }
}