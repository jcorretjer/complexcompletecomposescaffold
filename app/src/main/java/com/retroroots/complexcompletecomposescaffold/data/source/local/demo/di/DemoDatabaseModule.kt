package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.di

import android.content.Context
import androidx.room.Room
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DemoDatabaseModule
{
    @Singleton
    @Provides
    fun providesDemoDatabase(@ApplicationContext context: Context) =
            Room.databaseBuilder(context, DemoDatabase::class.java, DemoDatabase::class.simpleName)
                .build()
}