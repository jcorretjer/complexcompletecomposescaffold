package com.retroroots.complexcompletecomposescaffold.data.source.local.demo

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DemoData::class], version = 1, exportSchema = false)
abstract class DemoDatabase : RoomDatabase()
{
    abstract fun demoDao() : DemoDao
}