package com.retroroots.complexcompletecomposescaffold.data.source.local.di

import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDao
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDatabase
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedLocalDataSource
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.CheckedRepository
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.SharedCheckedRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object IODispatcherModule
{
    @Singleton
    @Provides
    fun providesIODispatcher() = Dispatchers.IO
}