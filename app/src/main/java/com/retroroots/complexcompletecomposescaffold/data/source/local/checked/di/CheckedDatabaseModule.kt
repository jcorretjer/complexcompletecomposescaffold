package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.di

import android.content.Context
import androidx.room.Room
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object CheckedDatabaseModule
{
    @Singleton
    @Provides
    fun providesCheckedDatabase(@ApplicationContext context: Context) =
            Room.databaseBuilder(context, CheckedDatabase::class.java, CheckedDatabase::class.simpleName)
                .build()
}