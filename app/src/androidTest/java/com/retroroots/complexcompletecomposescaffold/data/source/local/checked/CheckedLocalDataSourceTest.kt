package com.retroroots.complexcompletecomposescaffold.data.source.local.checked

import com.retroroots.complexcompletecomposescaffold.data.source.CheckedDataTest
import com.retroroots.complexcompletecomposescaffold.data.source.ViewModelTest
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.network.data_source.local.DatabaseResult
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class CheckedLocalDataSourceTest : CheckedDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var checkedLocalDataSource: CheckedLocalDataSource

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectInvalidIdTest()
    {
        runBlocking {
            assertTrue(checkedLocalDataSource.selectByDemoAndUserId(0, 0).first() is DatabaseResult.NotFound)
        }
    }

    private var demoId = 0

    @Test
    override fun selectValidIdTest()
    {
        demoId = 1

        runBlocking {

            createSomeDemoRecord(true, demoId)

            val data = checkedLocalDataSource.selectByDemoAndUserId(demoId, 1)
                .first()

            assertTrue(data is DatabaseResult.Found)

            val result = data as DatabaseResult.Found

            assertTrue(result.data.checked.and(result.data.id == 1).and(result.data.demoId == demoId))
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        demoId = 1

        runBlocking {
            createSomeDemoRecord(false, demoId)

            demoId = 2

            checkedLocalDataSource.updateByObj(CheckedData(1, true, demoId, 1))

            val data = checkedLocalDataSource.selectByDemoAndUserId(demoId, 1)
                .first()

            assertTrue(data is DatabaseResult.Found)

            val result = data as DatabaseResult.Found

            assertTrue(result.data.checked.and(result.data.id == 1).and(result.data.demoId == demoId))
        }
    }

    private suspend fun createSomeDemoRecord(checked: Boolean, demoId : Int)
    {
        checkedLocalDataSource.createByObj(CheckedData(1, checked, demoId, 1))
    }

    @Test
    override fun createValidRecordTest()
    {
        demoId = 2

        runBlocking {

            createSomeDemoRecord(false, demoId)

            val data = checkedLocalDataSource.selectByDemoAndUserId(demoId, 1)
                .first()

            assertTrue(data is DatabaseResult.Found)

            val result = data as DatabaseResult.Found

            assertTrue(result.data.checked.not().and(result.data.id == 1).and(result.data.demoId == demoId))
        }
    }
}