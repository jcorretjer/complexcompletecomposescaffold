package com.retroroots.complexcompletecomposescaffold.data.source.local.demo.di

import android.content.Context
import androidx.room.Room
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [DemoDatabaseModule::class])
@Module
object FakeDemoDatabaseModule
{
    @Singleton
    @Provides
    fun providesFakeDemoDatabase(@ApplicationContext context: Context) =
            Room.inMemoryDatabaseBuilder(context, DemoDatabase::class.java)
                .build()
}