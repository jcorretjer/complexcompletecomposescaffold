package com.retroroots.complexcompletecomposescaffold.data.source.local.checked.di

import android.content.Context
import androidx.room.Room
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.CheckedDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [CheckedDatabaseModule::class])
@Module
object FakeCheckedDatabaseModule
{
    @Singleton
    @Provides
    fun providesFakeCheckedDatabase(@ApplicationContext context: Context) =
            Room.inMemoryDatabaseBuilder(context, CheckedDatabase::class.java)
                .build()
}