package com.retroroots.complexcompletecomposescaffold.data.source.local

import com.retroroots.complexcompletecomposescaffold.data.source.DataTest

interface DemoDataTest : DataTest
{
   fun selectAllInvalidTest()

    fun selectAllValidTest()
}