package com.retroroots.complexcompletecomposescaffold.data.ui.compose

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.SemanticsProperties
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.test.SemanticsNodeInteraction
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.isNotDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import com.retroroots.complexcompletecomposescaffold.R
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.FakeCheckedRepo
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.ui.CheckedViewModel
import com.retroroots.complexcompletecomposescaffold.ui.screen.DataList
import com.retroroots.complexcompletecomposescaffold.ui.screen.TestTag
import com.retroroots.network.data_source.local.DatabaseResult
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import org.junit.Rule
import org.junit.Test

class DataListTest
{
    @get:Rule
    val rule = createComposeRule()

    private val emptyDemo = DemoData.empty()

    private val demos = listOf(emptyDemo.copy(id = 1), emptyDemo.copy(id = 2), emptyDemo.copy(id = 3))

    private val dataListNode = rule.onNodeWithTag(TestTag.DATA_LIST.name)

    private val notFoundNode = rule.onNodeWithTag(TestTag.NOT_FOUND.name)

    private val userId = 1

    @Test
    fun emptyListNotVisibleTest()
    {
        rule.setContent {
            DataList(
                demoResult = DatabaseResult.NotFound,
                userId = userId
            ) {
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        dataListNode.assertDoesNotExist()

        dataListNode.isNotDisplayed()

        notFoundNode.isDisplayed()
    }

    @Test
    fun populatedListVisibleTest()
    {
        rule.setContent {
            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        dataListNode.assertExists()

        dataListNode.isDisplayed()

        demos.forEach {
            rule.onNodeWithText(it.id.toString()).assertIsDisplayed()
        }
    }

    @Test
    fun click1stCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        chkBx.performClick()

        assertDemoCheckBox(chkBx, ToggleableState.On)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        assertDemoCheckBox(chkBx2, ToggleableState.Off)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        assertDemoCheckBox(chkBx3, ToggleableState.Off)
    }

    @Test
    fun click2ndCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        assertDemoCheckBox(chkBx, ToggleableState.Off)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        chkBx2.performClick()

        assertDemoCheckBox(chkBx2, ToggleableState.On)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        assertDemoCheckBox(chkBx3, ToggleableState.Off)
    }

    @Test
    fun click3rdCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        assertDemoCheckBox(chkBx, ToggleableState.Off)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        assertDemoCheckBox(chkBx2, ToggleableState.Off)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        chkBx3.performClick()

        assertDemoCheckBox(chkBx3, ToggleableState.On)
    }

    @Test
    fun click1stAnd2ndCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        chkBx.performClick()

        assertDemoCheckBox(chkBx, ToggleableState.On)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        chkBx2.performClick()

        assertDemoCheckBox(chkBx2, ToggleableState.On)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        assertDemoCheckBox(chkBx3, ToggleableState.Off)
    }

    @Test
    fun click2ndAnd3rdCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        assertDemoCheckBox(chkBx, ToggleableState.Off)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        chkBx2.performClick()

        assertDemoCheckBox(chkBx2, ToggleableState.On)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        chkBx3.performClick()

        assertDemoCheckBox(chkBx3, ToggleableState.On)
    }

    @Test
    fun click3rdAnd1stCheckboxSetsOnlyItToTrueTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        chkBx.performClick()

        assertDemoCheckBox(chkBx, ToggleableState.On)

        val chkBx2 = rule.onNodeWithTag("${demos[1].id}$chkBxTag")

        assertDemoCheckBox(chkBx2, ToggleableState.Off)

        val chkBx3 = rule.onNodeWithTag("${demos.last().id}$chkBxTag")

        chkBx3.performClick()

        assertDemoCheckBox(chkBx3, ToggleableState.On)
    }

    @Test
    fun selectUnselect1stCheckboxTest()
    {
        var chkBxTag = ""

        rule.setContent {

            chkBxTag = stringResource(id = R.string.checkBoxTestTegPrefix)

            DataList(
                demoResult = DatabaseResult.Found(demos),
                userId = userId
            ){
                CheckedViewModel(FakeCheckedRepo(), Dispatchers.IO)
            }
        }

        val chkBx = rule.onNodeWithTag("${demos.first().id}$chkBxTag")

        chkBx.performClick()

        rule.waitForIdle()

        chkBx.performClick()

        rule.waitForIdle()

        assertTrue(assertDemoCheckBox(chkBx, ToggleableState.Off))

        //assertDemoCheckBox(chkBx, ToggleableState.Off)
    }

    private fun assertDemoCheckBox(checkBox: SemanticsNodeInteraction, toggleableState: ToggleableState) : Boolean
    {
        for (item in checkBox.fetchSemanticsNode().config)
        {
            if (item.key.name == SemanticsProperties.ToggleableState.name)
            {
                //val assertion = item.value == toggleableState

                //assertTrue(item.value == toggleableState)

                return item.value == toggleableState
            }
        }

        return false
    }
}