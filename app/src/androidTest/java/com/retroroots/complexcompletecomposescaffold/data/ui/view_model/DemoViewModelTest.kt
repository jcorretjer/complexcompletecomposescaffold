package com.retroroots.complexcompletecomposescaffold.data.ui.view_model

import com.retroroots.complexcompletecomposescaffold.data.source.ViewModelTest
import com.retroroots.complexcompletecomposescaffold.data.source.local.DemoDataTest
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.DemoData
import com.retroroots.complexcompletecomposescaffold.data.source.local.demo.repo.FakeDemoRepo
import com.retroroots.complexcompletecomposescaffold.ui.DemoViewModel
import com.retroroots.network.data_source.local.DatabaseResult
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
@HiltAndroidTest
class DemoViewModelTest : DemoDataTest, ViewModelTest<List<DemoData>, DatabaseResult<List<DemoData>>>()
{
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val mainTestDispatcherRule = MainTestDispatcherRule()

    private val dispatcher = UnconfinedTestDispatcher()

    private lateinit var flow : Flow<DatabaseResult<List<DemoData>>>

    private val repo = FakeDemoRepo()

    private lateinit var viewModel: DemoViewModel

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    override fun initViewModel(dispatcher: TestDispatcher)
    {
        viewModel = DemoViewModel(repo, dispatcher)
    }

    private suspend fun  simplifiedCollectAndEmitFakeDBResults(
        testScope: TestScope,
        fakeDBResult: DatabaseResult<List<DemoData>>? = null,
        dispatcher: TestDispatcher = UnconfinedTestDispatcher(),
        suspendedAction : suspend CoroutineScope.() -> Unit
    )
    {
        collectAndEmitFakeDBResults(
            testScope,
            fakeDBResult,
            dispatcher,
            {
                fakeDBResult?.let {
                    repo.emitFakeDBResult(it)
                }
            },
            suspendedAction
        )
    }

    private lateinit var demoData: DemoData

    @Test
    override fun selectAllInvalidTest()
    {
        runTest {
            initViewModel(dispatcher)

            flow = viewModel.selectAll

            val results = mutableListOf<DatabaseResult<List<DemoData>>>()

            simplifiedCollectAndEmitFakeDBResults(this, DatabaseResult.NotFound){
                flow.toList(results)
            }

            assertTrue(results.first() is DatabaseResult.NotFound)
        }
    }

    @Test
    override fun selectAllValidTest()
    {
        demoData = DemoData.empty()

        runTest {
            initViewModel(dispatcher)

            flow = viewModel.selectAll

            val results = mutableListOf<DatabaseResult<List<DemoData>>>()

            simplifiedCollectAndEmitFakeDBResults(this, DatabaseResult.Found(listOf(demoData))){
                flow.toList(results)
            }

            val flowVal = results.first() as? DatabaseResult.Found

            assertTrue(flowVal!!.data.isNotEmpty().and(flowVal.data.first().id == demoData.id))
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        demoData = DemoData.empty()

        runTest {
            initViewModel(dispatcher)

            flow = viewModel.selectAll

            val results = mutableListOf<DatabaseResult<List<DemoData>>>()

            collectAndHandleDataSetModificationResult(
                this,
                listOf(demoData),
                dispatcher,
                {
                    viewModel.createRecord(it.first())
                },
                {
                    flow.toList(results)
                }
            )

            val result = results.first() as? DatabaseResult.Found

            assertTrue((result!!.data.first().id == demoData.id))
        }
    }
}