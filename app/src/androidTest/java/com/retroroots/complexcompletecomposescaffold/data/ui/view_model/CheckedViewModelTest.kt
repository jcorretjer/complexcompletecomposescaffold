package com.retroroots.complexcompletecomposescaffold.data.ui.view_model

import com.retroroots.complexcompletecomposescaffold.data.source.CheckedDataTest
import com.retroroots.complexcompletecomposescaffold.data.source.ViewModelTest
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.model.CheckedData
import com.retroroots.complexcompletecomposescaffold.data.source.local.checked.repo.FakeCheckedRepo
import com.retroroots.complexcompletecomposescaffold.ui.CheckedViewModel
import com.retroroots.network.data_source.local.DatabaseResult
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
@HiltAndroidTest
class CheckedViewModelTest: CheckedDataTest, ViewModelTest<CheckedData, DatabaseResult<CheckedData>>()
{
    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val mainTestDispatcherRule = MainTestDispatcherRule()

    private val dispatcher = UnconfinedTestDispatcher()

    private lateinit var flow : Flow<DatabaseResult<CheckedData>>

    private val repo = FakeCheckedRepo()

    private lateinit var viewModel: CheckedViewModel

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    override fun initViewModel(dispatcher: TestDispatcher)
    {
        viewModel = CheckedViewModel(repo, dispatcher)
    }

    private suspend fun  simplifiedCollectAndEmitFakeDBResults(
        testScope: TestScope,
        fakeDBResult: DatabaseResult<CheckedData>? = null,
        dispatcher: TestDispatcher = UnconfinedTestDispatcher(),
        suspendedAction : suspend CoroutineScope.() -> Unit
    )
    {
        collectAndEmitFakeDBResults(
            testScope,
            fakeDBResult,
            dispatcher,
            {
                fakeDBResult?.let {
                    repo.emitFakeDBResult(it)
                }
            },
            suspendedAction
        )
    }

    @Test
    override fun selectInvalidIdTest()
    {
        runTest {
            initViewModel(dispatcher)

            flow = viewModel.getCheckedByDemoAndUserId(-1, -1)

            val results = mutableListOf<DatabaseResult<CheckedData>>()

            simplifiedCollectAndEmitFakeDBResults(this, DatabaseResult.NotFound){
                    flow.toList(results)
            }

            assertTrue(results.first() is DatabaseResult.NotFound)
        }
    }

    private lateinit var checkedData: CheckedData

    @Test
    override fun selectValidIdTest()
    {
        checkedData = CheckedData.emptyData().copy(demoId = 1)

        runTest {
            initViewModel(dispatcher)

            flow = viewModel.getCheckedByDemoAndUserId(checkedData.demoId, checkedData.id)

            val results = mutableListOf<DatabaseResult<CheckedData>>()

            simplifiedCollectAndEmitFakeDBResults(this, DatabaseResult.Found(checkedData)){
                flow.toList(results)
            }

            val result = results.first() as? DatabaseResult.Found

            assertTrue(result!!.data.demoId == checkedData.demoId)
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        checkedData = CheckedData.emptyData().copy(checked = true, demoId = 2)

        runTest {
            initViewModel(dispatcher)

            flow = viewModel.getCheckedByDemoAndUserId(checkedData.demoId, checkedData.id)

            val results = mutableListOf<DatabaseResult<CheckedData>>()

            collectAndHandleDataSetModificationResult(this,
                checkedData,
                dispatcher,
                {
                    checkedData = checkedData.copy(checked = false).apply {
                        viewModel.updateCheckedRecord(this)
                    }
                },
                {
                    flow.toList(results)
                }
            )

            val result = results.first() as? DatabaseResult.Found

            assertTrue((result!!.data.demoId == checkedData.demoId).and(result.data.checked.not()))
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        checkedData = CheckedData.emptyData().copy(checked = true, demoId = 2)

        runTest {
            initViewModel(dispatcher)

            flow = viewModel.getCheckedByDemoAndUserId(checkedData.demoId, checkedData.id)

            val results = mutableListOf<DatabaseResult<CheckedData>>()

            collectAndHandleDataSetModificationResult(
                this,
                checkedData,
                dispatcher,
                {
                    viewModel.createCheckedRecord(it)
                },
                {
                    flow.toList(results)
                }
            )

            val result = results.first() as? DatabaseResult.Found

            assertTrue((result!!.data.demoId == checkedData.demoId).and(result.data.checked))
        }
    }
}