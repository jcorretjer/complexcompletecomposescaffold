package com.retroroots.complexcompletecomposescaffold.data.source

interface CheckedDataTest : DataTest
{
    fun selectInvalidIdTest()

    fun selectValidIdTest()

    fun updateValidRecordTest()
}