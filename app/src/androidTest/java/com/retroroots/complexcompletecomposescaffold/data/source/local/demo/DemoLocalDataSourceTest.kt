package com.retroroots.complexcompletecomposescaffold.data.source.local.demo

import com.retroroots.complexcompletecomposescaffold.data.source.local.DemoDataTest
import com.retroroots.network.data_source.local.DatabaseResult
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class DemoLocalDataSourceTest : DemoDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var demoLocalDataSource: DemoLocalDataSource

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectAllInvalidTest()
    {
        runBlocking {
            assertTrue(demoLocalDataSource.selectAll().first() is DatabaseResult.NotFound)
        }
    }

    private fun createSomeDemoRecord()
    {
        runBlocking {
            demoLocalDataSource.createByObj(DemoData.empty())
        }
    }

    @Test
    override fun selectAllValidTest()
    {
        createSomeDemoRecord()

        createSomeDemoRecord()

        runBlocking {

            val dataList = (demoLocalDataSource.selectAll()
                .first() as? DatabaseResult.Found)?.data

            assertNotNull(dataList)

            assertTrue(dataList!!.isNotEmpty())

            assertTrue(dataList[0].id == 1)

            assertTrue(dataList[1].id == 2)
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        createSomeDemoRecord()

        runBlocking {

            val demoData = (demoLocalDataSource.selectAll()
                .first() as? DatabaseResult.Found)?.data

            assertNotNull(demoData)

            assertTrue(demoData!!.isNotEmpty())

            assertTrue(demoData[0].id == 1)
        }
    }
}