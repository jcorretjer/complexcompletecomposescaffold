package com.retroroots.complexcompletecomposescaffold.data.source

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.TestScope

abstract class ViewModelTest<FakeDBResultType, FakeDBEmittedResultType>
{
    abstract fun initViewModel(dispatcher: TestDispatcher)

    private fun setupFlowCollection(
        testScope: TestScope,
        dispatcher: TestDispatcher,
        suspendedAction : suspend CoroutineScope.() -> Unit
    )
    {
        testScope.backgroundScope.launch(dispatcher, block = suspendedAction)
    }

    fun collectAndHandleDataSetModificationResult(
        testScope: TestScope,
        fakeDBResult: FakeDBResultType,
        dispatcher: TestDispatcher,
        onModifyQueryResult : (FakeDBResultType) -> Unit,
        suspendedAction : suspend CoroutineScope.() -> Unit
    )
    {
        setupFlowCollection(testScope, dispatcher, suspendedAction)

        onModifyQueryResult(fakeDBResult)
    }

    suspend fun collectAndEmitFakeDBResults(
        testScope: TestScope,
        fakeDBResult: FakeDBEmittedResultType? = null,
        dispatcher: TestDispatcher,
        onEmit : suspend (FakeDBEmittedResultType?) -> Unit,
        suspendedAction : suspend CoroutineScope.() -> Unit
    )
    {
        setupFlowCollection(testScope, dispatcher, suspendedAction)

        onEmit(fakeDBResult)
    }
}