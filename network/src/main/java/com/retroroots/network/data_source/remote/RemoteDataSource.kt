package com.retroroots.network.data_source.remote

import com.retroroots.network.data_source.DataSource
import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import retrofit2.Response
import java.net.UnknownHostException

abstract class RemoteDataSource<ArgumentType>: DataSource<ArgumentType, ResponseState<ArgumentType>>
{
    protected open fun <ResponseType> processResponse(process : Response<ResponseType>) : ResponseState<ResponseType>
    {
        process.apply {
            body()?.let {
                if(isSuccessful)
                    return ResponseState.Success(it)
            }

            return ResponseState.Failure(ApiError.ServerError(Exception()))
        }
    }

    protected open fun processException(exception: Exception) : ResponseState<Nothing> =
            when(exception)
            {
                is UnknownHostException -> ResponseState.Failure(ApiError.NoInternetConnectionError(exception))

                else -> ResponseState.Failure(ApiError.UnknownServerError(exception))
            }
}