package com.retroroots.network.data_source.local

sealed class DatabaseResult<out Result>
{
    data class Found<out Result>(val data : Result) : DatabaseResult<Result>()

    data object NotFound : DatabaseResult<Nothing>()
}