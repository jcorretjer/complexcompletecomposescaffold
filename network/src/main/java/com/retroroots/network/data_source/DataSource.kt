package com.retroroots.network.data_source

interface DataSource<ArgumentType, ReturnType>
{
    suspend fun selectById(id : Int): ReturnType

    suspend fun updateByObj(obj : ArgumentType): ReturnType

    suspend fun createByObj(obj : ArgumentType): ReturnType
}