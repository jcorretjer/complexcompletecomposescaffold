package com.retroroots.network.data_source.local

import kotlinx.coroutines.flow.Flow

interface LocalDataSource<ArgumentType, ReturnType>
{
    /*fun selectById(id : Int): Flow<ReturnType>?

    fun selectAll(): Flow<List<ReturnType>>*/

    suspend fun updateByObj(obj : ArgumentType)

    suspend fun createByObj(obj : ArgumentType)
}