package com.retroroots.network.data_source.remote

import com.retroroots.network.data_source.remote.state.ResponseState
import retrofit2.Response

abstract class CompactRemoteDataSource<RawType, CompactType>: RemoteDataSource<CompactType>()
{
    abstract fun compressResponse(process : Response<RawType>) : ResponseState<CompactType>
}