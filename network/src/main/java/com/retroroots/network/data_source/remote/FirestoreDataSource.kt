package com.retroroots.network.data_source.remote

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await

//todo find a way to implement my out firestore timeout. Creo que lo mejor seria no empezar el request sino hay internet connection
abstract class FirestoreDataSource<DataType>
{
    protected suspend fun setData(
        fieldData: Any,
        docId: String? = null,
        collectionRef: CollectionReference
    ): ResponseState<Unit>
    {
        //Create new doc
        (if (docId == null)
            collectionRef.document()

        //Update doc
        else
            collectionRef.document(docId)).apply {
            return try
            {
                set(fieldData).await()

                ResponseState.Success(Unit)
            }
            catch (exception: Exception)
            {
                ResponseState.Failure(ApiError.ServerError(exception))
            }
        }
    }

    protected suspend fun selectDataById(docId: String, collectionRef: CollectionReference): ResponseState<DataType?> =
            try
            {
                ResponseState.Success(processDocSnapshot(collectionRef.document(docId).get().await()))
            }

            catch (exception: Exception)
            {
                ResponseState.Failure(ApiError.UnknownServerError(exception))
            }

    protected fun selectAllData(
        collectionRef: CollectionReference,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    )= collectionRef.toFlowSnapshot(onCompleted)

    private fun queryData(
        query: Query,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    ) = query.toFlowSnapshot(onCompleted)

    private fun queryPagedData(
        query: Query,
        groupByPredicament: (DataType?) -> String?,
        onCompleted: ((List<List<DataType?>>, List<DocumentSnapshot>) -> List<List<DataType?>>)? = null
    ) = query.toFlowPagedSnapshot(groupByPredicament, onCompleted)

    protected abstract fun processDocSnapshot(response: DocumentSnapshot): DataType?

    private fun processDocSnapshots(
        response: List<DocumentSnapshot>,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null): List<DataType?>
    {
        val docs = mutableListOf<DataType?>()

        response.forEach { doc ->
            docs.add(processDocSnapshot(doc))
        }

        return onCompleted?.invoke(docs, response) ?: docs
    }

    private fun processDocSnapshotsWithPaging(
        response: List<DocumentSnapshot>,
        groupByPredicament: (DataType?) -> String?,
        onCompleted: ((List<List<DataType?>>, List<DocumentSnapshot>) -> List<List<DataType?>>)? = null): List<List<DataType?>>
     {
        return mutableListOf<DataType?>().let { docs ->
            response.forEach { doc ->
                docs.add(processDocSnapshot(doc))
            }

            docs.groupBy(groupByPredicament)
                .values
                .toList()
                .let {
                onCompleted?.invoke(it, response) ?:
                it
            }
        }
    }

    protected fun selectWhereInBetween(
        key1: String,
        value1: Any,
        collectionRef: CollectionReference,
        key2: String,
        value2: Any,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    ) = queryData(collectionRef.whereGreaterThanOrEqualTo(key1, value1).whereLessThanOrEqualTo(key2, value2), onCompleted)

    protected fun selectWhereInBetweenPagedSorted(
        key1: String,
        value1: Any,
        collectionRef: CollectionReference,
        key2: String,
        value2: Any,
        groupByPredicament: (DataType?) -> String?,
        sortKey: String,
        sortDirection: Query.Direction,
        onCompleted: ((List<List<DataType?>>, List<DocumentSnapshot>) -> List<List<DataType?>>)? = null
    ) = queryPagedData(
        collectionRef.whereGreaterThanOrEqualTo(key1, value1)
            .whereLessThanOrEqualTo(key2, value2)
            .orderBy(sortKey, sortDirection),
        groupByPredicament,
        onCompleted
    )

    protected fun selectWhereEquals(
        key: String,
        value: Any,
        collectionRef: CollectionReference,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    ) = queryData(collectionRef.whereEqualTo(key, value), onCompleted)

    private fun Query.toFlowSnapshot(onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)?) =
            callbackFlow<ResponseState<List<DataType?>>> {
                addSnapshotListener { value, error ->
                    error?.let {
                        trySend(ResponseState.Failure(ApiError.UnknownServerError(it)))
                    } ?: run {
                            value?.let {
                                trySend(ResponseState.Success(processDocSnapshots(it.documents, onCompleted)))
                            } ?: run {
                                    trySend(ResponseState.Failure(ApiError.ServerError()))
                                }
                        }
                }

                awaitClose { }
            }

    private fun Query.toFlowPagedSnapshot(
        groupByPredicament: (DataType?) -> String?,
        onCompleted: ((List<List<DataType?>>, List<DocumentSnapshot>) -> List<List<DataType?>>)?
    ) = callbackFlow<ResponseState<List<List<DataType?>>>> {
                addSnapshotListener { value, error ->
                    error?.let {
                        trySend(ResponseState.Failure(ApiError.UnknownServerError(it)))
                    } ?: run {
                        value?.let {
                            trySend(ResponseState.Success(processDocSnapshotsWithPaging(it.documents, groupByPredicament, onCompleted)))
                        } ?: run {
                            trySend(ResponseState.Failure(ApiError.ServerError()))
                        }
                    }
                }

                awaitClose { }
            }

    protected fun selectWhereGreaterThanOrEqualTo(
        key: String,
        value: Any,
        collectionRef: CollectionReference,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    ) = queryData(collectionRef.whereGreaterThanOrEqualTo(key, value), onCompleted)

    protected fun selectAllDataOrderByKey(
        key: String,
        sortDirection: Query.Direction,
        collectionRef: CollectionReference,
        onCompleted: ((List<DataType?>, List<DocumentSnapshot>) -> List<DataType?>)? = null
    )= collectionRef.orderBy(key, sortDirection).toFlowSnapshot(onCompleted)

    protected fun selectAllDataPagedOrderByKey(
        key: String,
        sortDirection: Query.Direction,
        collectionRef: CollectionReference,
        groupByPredicament: (DataType?) -> String?,
        onCompleted: ((List<List<DataType?>>, List<DocumentSnapshot>) -> List<List<DataType>>)? = null
    )= collectionRef.orderBy(key, sortDirection).toFlowPagedSnapshot(groupByPredicament, onCompleted)
}