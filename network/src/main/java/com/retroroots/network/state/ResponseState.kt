package com.retroroots.network.state

sealed class ResponseState<out S>
{
    data class Success<out S>(val data : S) : ResponseState<S>()

    data class Failure<out S>(val error : ApiError) : ResponseState<S>()

    data object Loading : ResponseState<Nothing>()
}
