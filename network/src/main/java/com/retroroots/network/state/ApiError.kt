package com.retroroots.network.state

import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response

const val SERVER_ERROR_MESSAGE = "server error"

sealed class ApiError(override val exception: Exception) : Error
{
    class NoInternetConnectionError(override val exception: Exception) : Error, ApiError(exception)

    companion object
    {
        val SERVER_ERROR_EXCEPTION = HttpException(
            Response.error<ResponseBody>
                (
                500,
                ResponseBody.create(
                    MediaType.parse("plain/text"),
                    SERVER_ERROR_MESSAGE
                )
            )
        )
    }

    class ServerError(override val exception: Exception = Exception(SERVER_ERROR_MESSAGE)) : Error, ApiError(exception)

    class UnknownServerError(override val exception: Exception) : Error, ApiError(exception)
}

sealed interface Error
{
    val exception: Exception
}